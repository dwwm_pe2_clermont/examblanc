import {Marque} from "./marque";

export class VoitureForm {
  id?: number;
  image?: string;
  marque?: string;
  nom?: string;
  nbKm?: number;
  prix?: number;



  constructor(id?: number, image?: string, marque?: string, nom?: string, nbKm?: number, prix?: number) {
    this.id = id;
    this.image = image;
    this.marque = marque;
    this.nom = nom;
    this.nbKm = nbKm;
    this.prix = prix
  }
}
