import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment.development";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {catchError, Observable, retry, throwError} from "rxjs";
import {Voiture} from "../models/voiture";
import {VoitureForm} from "../models/voitureForm";

@Injectable({
  providedIn: 'root'
})
export class VoitureService {

  apiUrl = environment.apiUrl;

  constructor(private httpClient: HttpClient) { }

  getAll(): Observable<Voiture[]>{
    return this.httpClient.get<Voiture[]>(this.apiUrl+"/api/voitures").pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  put(vehicule: VoitureForm): Observable<Voiture>{
    return this.httpClient.put<Voiture>(this.apiUrl+"/api/voitures/"+vehicule.id, vehicule).pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  post(voiture: VoitureForm): Observable<Voiture>{
    return this.httpClient.post<Voiture>(this.apiUrl+"/api/voitures", voiture).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  getOne(id: number): Observable<Voiture>{
    return this.httpClient.get<Voiture>(this.apiUrl+"/api/voitures/"+id).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  delete(id: number): Observable<Voiture>{
    return this.httpClient.delete<Voiture>(this.apiUrl+'/api/voitures/'+id).pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }
    // Return an observable with a user-facing error message.
    return throwError(() => new ErrorEvent(error.error["hydra:description"]));
  }
}
