import { Routes } from '@angular/router';
import {HomeComponent} from "./components/public/home/home.component";
import {DetailComponent} from "./components/public/detail/detail.component";
import {LoginComponent} from "./components/admin/login/login.component";
import {AdminListComponent} from "./components/admin/admin-list/admin-list.component";
import {AjoutComponent} from "./components/admin/ajout/ajout.component";
import {EditComponent} from "./components/admin/edit/edit.component";
import {adminGuard} from "./guards/admin.guard";

export const routes: Routes = [
  {
    path: '', component: HomeComponent,
  },
  {
    path: 'login', component: LoginComponent
  },
  {
    path: 'admin', component: AdminListComponent, canActivate: [adminGuard]
  },
  {
    path: 'admin/ajout', component: AjoutComponent, canActivate: [adminGuard]
  },
  {
    path: 'admin/edit/:id', component: EditComponent, canActivate: [adminGuard]
  },
  {
    path: ':id', component: DetailComponent
  }

];
