import {Component, OnInit} from '@angular/core';
import {FormsModule} from "@angular/forms";
import {Voiture} from "../../../models/voiture";
import {VoitureForm} from "../../../models/voitureForm";
import {CommonModule} from "@angular/common";
import {MarqueService} from "../../../services/marque.service";
import {Marque} from "../../../models/marque";
import {VoitureService} from "../../../services/voiture.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-ajout',
  standalone: true,
  imports: [FormsModule, CommonModule],
  templateUrl: './ajout.component.html',
  styleUrl: './ajout.component.css'
})
export class AjoutComponent implements OnInit{

  isLoading = true;

  vehicule = new VoitureForm();
  marques?: Marque[];
  stringError?: string;

  constructor(private marqueService: MarqueService, private vehiculeService: VoitureService, private router: Router) {
  }

  ajoutVehicule() {
    this.vehiculeService.post(this.vehicule).subscribe(data => {
      this.router.navigate(["/admin"]);
    }, error => {

    })
  }

  ngOnInit(): void {
    this.marqueService.getAll().subscribe(data => {
      this.marques = data;
      this.isLoading = false;
    })
  }
}
