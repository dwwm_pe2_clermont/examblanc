import {Component, OnInit} from '@angular/core';
import {VoitureService} from "../../../services/voiture.service";
import {MarqueService} from "../../../services/marque.service";
import {VoitureForm} from "../../../models/voitureForm";
import {Voiture} from "../../../models/voiture";
import {Marque} from "../../../models/marque";
import {ActivatedRoute, Router, RouterModule} from "@angular/router";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";

@Component({
  selector: 'app-edit',
  standalone: true,
  imports: [CommonModule, FormsModule, RouterModule],
  templateUrl: './edit.component.html',
  styleUrl: './edit.component.css'
})
export class EditComponent implements OnInit{
  isLoading: boolean = true;
  vehicule?: Voiture;
  vehiculeForm: VoitureForm = new VoitureForm();
  marques?: Marque[];


  constructor(private voitureService: VoitureService, private marqueService: MarqueService,
              private activatedRoute: ActivatedRoute, private routerService: Router) {
  }

  ngOnInit(): void {
    this.marqueService.getAll().subscribe(data => {
      this.marques = data;
      let id = this.activatedRoute.snapshot.paramMap.get('id');
      if(id){
        this.voitureService.getOne(+id).subscribe(data => {
          this.vehicule = data;
          this.vehiculeForm.id = data.id;
          this.vehiculeForm.marque = "/api/marques/"+data.marque.id;
          this.vehiculeForm.image = data.image;
          this.vehiculeForm.prix = data.prix;
          this.vehiculeForm.nbKm = data.nbKm;
          this.vehiculeForm.nom = data.nom;
          this.isLoading = false;
        })
      }

    })
  }

  updateVehicule() {
    this.isLoading = true;
    this.voitureService.put(this.vehiculeForm).subscribe(data => {
      this.routerService.navigate(["/admin"]);
      this.isLoading = false;
    })
  }
}
