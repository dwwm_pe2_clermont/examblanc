import {Component, inject, OnInit} from '@angular/core';
import {Router, RouterModule} from "@angular/router";
import {VoitureService} from "../../../services/voiture.service";
import {Voiture} from "../../../models/voiture";
import {CommonModule} from "@angular/common";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {DeleteConfirmService} from "../../../services/delete-confirm.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-admin-list',
  standalone: true,
  imports: [CommonModule, RouterModule],
  templateUrl: './admin-list.component.html',
  styleUrl: './admin-list.component.css'
})
export class AdminListComponent implements OnInit{

  isLoading = true;
  voitures?: Voiture[];

  constructor(private router: Router, private voitureService: VoitureService,
              private confirmSercice: DeleteConfirmService, private toastr: ToastrService) {
  }
  logout() {
    window.localStorage.removeItem("token");
    this.router.navigate(["/login"]);
  }

  ngOnInit(): void {
    this.voitureService.getAll().subscribe(data => {
      this.voitures = data;
      this.isLoading = false;
    })
  }

  deleteEvent(id: number) {
    this.isLoading = true;
    this.confirmSercice
      .confirm("Veuillez confirmer",
        "Action irrémédiable" ).then(res => {
     if(res){
      this.voitureService.delete(id).subscribe(data => {
        this.ngOnInit();
        this.isLoading = false;
        this.toastr.success("Véhicule supprimé")
      })
     }
    });
  }
}
