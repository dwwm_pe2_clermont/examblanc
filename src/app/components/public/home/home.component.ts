import {Component, OnInit} from '@angular/core';
import {VoitureService} from "../../../services/voiture.service";
import {MarqueService} from "../../../services/marque.service";
import {Voiture} from "../../../models/voiture";
import {Marque} from "../../../models/marque";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [CommonModule, RouterModule],
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent implements OnInit{
  voitures?: Voiture[];
  isLoading = true;

  constructor(private voitureService: VoitureService,
              private marqueService: MarqueService) {
  }

  ngOnInit() {
      this.voitureService.getAll().subscribe(data => {
        this.voitures = data;
        this.isLoading = false;
      });

  }

}
