import {Component, OnInit} from '@angular/core';
import {VoitureService} from "../../../services/voiture.service";
import {Voiture} from "../../../models/voiture";
import {ActivatedRoute, RouterModule} from "@angular/router";
import {CommonModule} from "@angular/common";

@Component({
  selector: 'app-detail',
  standalone: true,
  imports: [CommonModule, RouterModule],
  templateUrl: './detail.component.html',
  styleUrl: './detail.component.css'
})
export class DetailComponent implements OnInit{

  voiture?: Voiture;
  isLoading: boolean = true;

  constructor(private voitureService: VoitureService, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    if(id){
      this.voitureService.getOne(+id).subscribe(data => {
        this.voiture = data;
        this.isLoading = false;
      })
    }

  }

}
